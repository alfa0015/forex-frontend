export const firstMixin = {
  methods: {
    auth () {
      if (localStorage.getItem('access-token') == null || localStorage.getItem('client') == null || localStorage.getItem('expiry') == null || localStorage.getItem('uid') == null) {
        return false
      }
    },
    token (headers) {
      if (localStorage.getItem('access-token') == null || localStorage.getItem('client') == null || localStorage.getItem('expiry') == null || localStorage.getItem('uid') == null) {
        localStorage.setItem('access-token', headers['access-token'])
        localStorage.setItem('client', headers.client)
        localStorage.setItem('expiry', headers.expiry)
        localStorage.setItem('uid', headers.uid)
      } else {
        if (localStorage.getItem('access-token') !== headers['access-token']) {
          localStorage.setItem('access-token', headers['access-token'])
        }
        if (localStorage.getItem('client') !== headers.client) {
          localStorage.setItem('client', headers.client)
        }
        if (localStorage.getItem('expiry') !== headers.expiry) {
          localStorage.setItem('expiry', headers.expiry)
        }
        if (localStorage.getItem('uid') !== headers.uid) {
          localStorage.setItem('uid', headers.uid)
        }
      }
    }
  }
}
