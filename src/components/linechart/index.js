import { Line } from 'vue-chartjs'
// import the component - chart you need

export default {
  extends: Line,
  props: ['data', 'options'],
  mounted () {
    this.renderChart(this.data, this.options)
  }
}
