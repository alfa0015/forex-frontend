import { Radar } from 'vue-chartjs'
// import the component - chart you need

export default {
  extends: Radar,
  props: ['data', 'options'],
  mounted () {
    this.renderChart(this.data, this.options)
  }
}
