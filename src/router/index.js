import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login'
import Dashboard from '@/components/dashboard/index'
import Users from '@/components/users'
import UserProfile from '@/components/userprofile'
import Account from '@/components/account'
import Permissions from '@/components/permissions'
import Groups from '@/components/groups'
import Renovations from '@/components/renovations'
import Pips from '@/components/pips'
import Retreats from '@/components/retreats'
import Deposits from '@/components/deposits'
import Risks from '@/components/risks'
import GroupNew from '@/components/groups/new'
import GroupEdit from '@/components/groups/edit'
import userNew from '@/components/users/new'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/users',
      name: 'users',
      component: Users
    },
    {
      path: '/users/:id',
      name: 'profile',
      component: UserProfile,
      props: true
    },
    {
      path: '/users/new',
      name: 'userNew',
      component: userNew
    },
    {
      path: '/account/:id',
      name: 'account',
      component: Account,
      props: true
    },
    {
      path: '/groups',
      name: 'groups',
      component: Groups
    },
    {
      path: '/groups/new',
      name: 'newgroup',
      component: GroupNew
    },
    {
      path: '/groups/:id/edit',
      name: 'editgroup',
      component: GroupEdit,
      props: true
    },
    {
      path: '/permissions',
      name: 'permissions',
      component: Permissions
    },
    {
      path: '/renovations',
      name: 'renovations',
      component: Renovations
    },
    {
      path: '/pips',
      name: 'pips',
      component: Pips
    },
    {
      path: '/retreats',
      name: 'retreats',
      component: Retreats
    },
    {
      path: '/deposits',
      name: 'deposits',
      component: Deposits
    },
    {
      path: '/risks',
      name: 'risks',
      component: Risks
    }
  ]
})
